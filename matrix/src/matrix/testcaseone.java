package matrix;

import java.io.FileInputStream;
//import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class testcaseone {

	
	private WebDriver driver;
	@Before
	public void get() throws IOException
	{
		
		Properties p=new Properties();
		FileInputStream f=new FileInputStream("C:\\Users\\Ananthanarayanan\\workspace\\matrix\\efiling.properties");
		p.load(f);
		driver=new FirefoxDriver();
		driver.get(p.getProperty("url"));
		driver.manage().window().maximize();
		
	}
	
	
	
	
	
	@SuppressWarnings({ "rawtypes", "unused" })
	@Test
	public void test() throws IOException, InterruptedException
	{
		Properties p=new Properties();
		FileInputStream f=new FileInputStream("C:\\Users\\Ananthanarayanan\\workspace\\matrix\\efiling.properties");
		p.load(f);
		//User login		
				driver.get(p.getProperty("url"));
				driver.findElement(By.xpath(p.getProperty("uname"))).sendKeys("test.user2@matrixcos.com");
				driver.findElement(By.xpath(p.getProperty("pwd"))).sendKeys("Test1234");
				driver.findElement(By.cssSelector(p.getProperty("clk1"))).click();
				driver.findElement(By.cssSelector(p.getProperty("clk2"))).click();
				driver.findElement(By.xpath(p.getProperty("clk3"))).click();


				
				
				//Transfer control to new window
				Set windowHandles = driver.getWindowHandles();
				Iterator it = windowHandles.iterator();
				String parentBrowser= (String) it.next();
				String childBrowser = (String) it.next();
				driver.switchTo().window(childBrowser);
				
				
				
				
				
				//search hr data
				driver.findElement(By.id(p.getProperty("id"))).sendKeys("648");
				Thread.sleep(1000);
				driver.findElement(By.xpath(p.getProperty("lname"))).sendKeys("as");
				Thread.sleep(1000);
				driver.findElement(By.xpath(p.getProperty("datepick"))).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath(p.getProperty("date"))).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath(p.getProperty("search"))).click();
				
		        
				
				
				
				//selecting the claimant
				Thread.sleep(2000);
				WebElement element4=driver.findElement(By.xpath(p.getProperty("select")));
				Actions act=new Actions(driver);
				act.moveToElement(element4);
				act.build().perform();
				element4.click();

				
				
				
				
				//selecting the leave type		
				Thread.sleep(2000);
				driver.findElement(By.xpath(p.getProperty("lclick1"))).click();
				Thread.sleep(2000);
				WebElement element6=driver.findElement(By.xpath(p.getProperty("dpdwn")));
				Select cust = new Select(element6);
				Thread.sleep(2000);
				cust.selectByIndex(2);
				element6.click();		
				Thread.sleep(2000);		
				driver.findElement(By.xpath(p.getProperty("lclick2"))).click();
				Thread.sleep(2000);
				driver.findElement(By.xpath(p.getProperty("lclick3"))).click();
				
				//filing intake
					driver.findElement(By.xpath(p.getProperty("flname"))).sendKeys("ads87.,");
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(p.getProperty("ffname"))).sendKeys("addss87.,");
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(p.getProperty("myeacc"))).click();
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(p.getProperty("reasforleav"))).sendKeys("fever");
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(p.getProperty("datepic"))).click();
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(p.getProperty("datecurr"))).click();
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(p.getProperty("todet"))).click();
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(p.getProperty("reqleave"))).click();
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(p.getProperty("person"))).click();
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(p.getProperty("allhealth"))).click();
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(p.getProperty("warmtran"))).click();
					Thread.sleep(2000);	
					
					driver.findElement(By.xpath(p.getProperty("next"))).click();				
					
		}
				
@After
public void print()
{
	System.out.println("Test case successfully executed");
}
	
	
	}

